package ru.t1.godyna.tm.exception.field;

public final class NumberIncorrectException extends AbsractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(final String value) {
        super("Error! This value '" + value + "' is incorrect...");
    }

}
