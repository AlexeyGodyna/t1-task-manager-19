package ru.t1.godyna.tm.exception.field;

public final class PasswordEmptyException extends AbsractFieldException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}
