package ru.t1.godyna.tm.model;

import ru.t1.godyna.tm.api.model.IWBS;
import ru.t1.godyna.tm.enumerated.Status;

import java.util.Date;

public final class Project extends AbstractModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    public Project() {
    }

    public Project(final String name, final Status status) {
        this.name = name;
        this.status = status;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public Status getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}
