package ru.t1.godyna.tm.repository;

import ru.t1.godyna.tm.api.repository.IUserRepository;
import ru.t1.godyna.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (User user : records) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : records) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public Boolean isMailExist(final String email) {
        return findByEmail(email) != null;
    }

}
