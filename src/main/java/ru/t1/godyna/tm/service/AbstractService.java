package ru.t1.godyna.tm.service;

import ru.t1.godyna.tm.api.repository.IRepository;
import ru.t1.godyna.tm.api.service.IService;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.exception.entity.EntityNotFoundException;
import ru.t1.godyna.tm.exception.field.IdEmptyException;
import ru.t1.godyna.tm.exception.field.IndexIncorrectException;
import ru.t1.godyna.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository <M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    public M add(final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.add(model);
        return model;
    }

    public List<M> findAll(){
        return repository.findAll();
    }

    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    public void remove(final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.remove(model);
    }

    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        final M model = repository.findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

}
