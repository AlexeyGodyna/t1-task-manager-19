package ru.t1.godyna.tm.service;

import ru.t1.godyna.tm.api.repository.IUserRepository;
import ru.t1.godyna.tm.api.service.IUserService;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.exception.entity.UserNotFoundException;
import ru.t1.godyna.tm.exception.field.EmailEmptyException;
import ru.t1.godyna.tm.exception.field.LoginEmptyException;
import ru.t1.godyna.tm.exception.field.PasswordEmptyException;
import ru.t1.godyna.tm.exception.user.ExistsEmailException;
import ru.t1.godyna.tm.exception.user.ExistsLoginException;
import ru.t1.godyna.tm.model.User;
import ru.t1.godyna.tm.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (email == null || email.isEmpty()) throw new ExistsEmailException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        User user = repository.findByLogin(login);
        if (user == null) return null;
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new ExistsEmailException();
        User user = repository.findByEmail(email);
        if (user == null) return null;
        return user;
    }

    @Override
    public void removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        remove(user);
    }

    @Override
    public void removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findByEmail(email);
        remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    ) {
        final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.isLoginExist(login);
    }

    @Override
    public Boolean isMailExist(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.isMailExist(email);
    }

}
