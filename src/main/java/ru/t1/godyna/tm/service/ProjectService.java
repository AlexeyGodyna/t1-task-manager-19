package ru.t1.godyna.tm.service;

import ru.t1.godyna.tm.api.repository.IProjectRepository;
import ru.t1.godyna.tm.api.service.IProjectService;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.ProjectNotFoundException;
import ru.t1.godyna.tm.exception.entity.StatusEmptyException;
import ru.t1.godyna.tm.exception.field.DescriptionEmptyException;
import ru.t1.godyna.tm.exception.field.IndexIncorrectException;
import ru.t1.godyna.tm.exception.field.NameEmptyException;
import ru.t1.godyna.tm.exception.field.ProjectIdEmptyException;
import ru.t1.godyna.tm.model.Project;

public final class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        if (description == null)
            throw new DescriptionEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        final Project project = new Project();
        project.setName(name);
        add(project);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        final Project project = findOneById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= repository.getSize())
            throw new IndexIncorrectException();
        if (status == null)
            throw new StatusEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty())
            throw new ProjectIdEmptyException();
        if (status == null)
            throw new StatusEmptyException();
        final Project project = findOneById(id);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
