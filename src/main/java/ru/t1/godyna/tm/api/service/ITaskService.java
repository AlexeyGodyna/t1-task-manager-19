package ru.t1.godyna.tm.api.service;

import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
