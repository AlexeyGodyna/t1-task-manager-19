package ru.t1.godyna.tm.api.repository;

import ru.t1.godyna.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task create(String name, String description);

    Task create(String name);

    List findAllByProjectId(String projectId);

}
