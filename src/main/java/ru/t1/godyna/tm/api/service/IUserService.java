package ru.t1.godyna.tm.api.service;

import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    void removeByLogin(String login);

    void removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

    Boolean isLoginExist(String login);

    Boolean isMailExist(String email);

}
