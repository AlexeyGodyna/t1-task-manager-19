package ru.t1.godyna.tm.api.repository;

import ru.t1.godyna.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository <M extends AbstractModel> {

    M add(M model);

    void remove(M model);

    void clear();

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M removeById(String id);

    M removeByIndex(Integer index);

    Integer getSize();

}
