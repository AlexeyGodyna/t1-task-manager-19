package ru.t1.godyna.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    private final String NAME = "project-clear";

    private final String DESCRIPTION = "Remove all projects.";

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        getProjectService().clear();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
