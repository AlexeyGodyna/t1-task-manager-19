package ru.t1.godyna.tm.command.user;

import ru.t1.godyna.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    private final String NAME = "user-registry";

    private final String DESCRIPTION = "registry user.";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        getAuthService().registry(login, password, email);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
