package ru.t1.godyna.tm.command.project;

import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.ProjectNotFoundException;
import ru.t1.godyna.tm.model.Project;

public abstract class AbstractProjectShowCommand extends AbstractProjectCommand {

    public void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}
