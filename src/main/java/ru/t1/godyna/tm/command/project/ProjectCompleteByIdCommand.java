package ru.t1.godyna.tm.command.project;

import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    private final String NAME = "project-completed-by-id";

    private final String DESCRIPTION = "Complete project by id.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
