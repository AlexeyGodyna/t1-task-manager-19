package ru.t1.godyna.tm.command.project;

import ru.t1.godyna.tm.model.Project;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectShowCommand {

    private final String NAME = "project-show-by-index";

    private final String DESCRIPTION = "Display project by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        showProject(project);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
