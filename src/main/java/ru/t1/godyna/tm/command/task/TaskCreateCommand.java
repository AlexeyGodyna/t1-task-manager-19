package ru.t1.godyna.tm.command.task;

import ru.t1.godyna.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    private final String NAME = "task-create";

    private final String DESCRIPTION = "Create new task.";

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
