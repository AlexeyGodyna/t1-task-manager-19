package ru.t1.godyna.tm.command.project;

import ru.t1.godyna.tm.model.Project;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectShowCommand {

    private final String NAME = "project-show-by-id";

    private final String DESCRIPTION = "Display project by id.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        showProject(project);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
