package ru.t1.godyna.tm.command.task;

import ru.t1.godyna.tm.api.service.IProjectTaskService;
import ru.t1.godyna.tm.api.service.ITaskService;
import ru.t1.godyna.tm.command.AbstractCommand;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

}
