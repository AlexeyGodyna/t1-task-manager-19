package ru.t1.godyna.tm.command.system;

import ru.t1.godyna.tm.api.model.ICommand;
import ru.t1.godyna.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationListCommand extends AbstractSystemCommand {

    private final String NAME = "commands";

    private final String ARGUMENT = "-cmd";

    private final String DESCRIPTION = "Show command list.";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (ICommand command: commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name.toString());
        }
    }

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Show command list.";
    }

}
